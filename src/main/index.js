'use strict'

import { app, BrowserWindow, globalShortcut, ipcMain } from 'electron'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

// 关闭警告
process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true

let mainWindow, winprintp // winprints
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000
    // webPreferences: {
    //   webSecurity: false
    // },
    // useContentSize: true,
    // autoHideMenuBar: true,
    // show:false,
    // frame: false
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)
app.on('ready', printBrowser)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
    printBrowser()
  }
})

// 快捷键设置
app.on('ready', () => {
  const ret = globalShortcut.register('Alt+N+M', () => {
    mainWindow.webContents.send('pingpong', 'data')
  })
  if (!ret) {
    console.log('失败')
  }
})
app.on('will-quit', () => {
  globalShortcut.unregisterAll()
})

// 打印方法一 -->开始
function printBrowser () {
  winprintp = new BrowserWindow({
    width: 1024,
    height: 768,
    frame: false,
    show: false,
    // backgroundColor: '#4b5b79',
    minWidth: 1024,
    minHeight: 768,
    webPreferences: {
      webSecurity: false
    }
    // x: 1500,
    // y: 0
  })
  const printURL = process.env.NODE_ENV === 'development'
    ? `http://localhost:9080/#/views/printhander`
    : `file://${__dirname}/index.html#printhander`

  winprintp.loadURL(printURL)
  winprintp.on('closed', () => {
    winprintp = null
  })
}
// 打印方法一 打印事件
ipcMain.on('main-do-print', (e, d) => {
  winprintp.webContents.send('main-send-msg', d)
  winprintp.webContents.executeJavaScript('window.print()')
})
// 往打印窗口发送预览资源--请求指令
ipcMain.on('main-review-print', () => {
  winprintp.webContents.send('main-view-print')
})
// 往主窗口发送预览资源--传送指令
ipcMain.on('print-view-ok', (e, d) => {
  mainWindow.webContents.send('print-view-source', d)
})
// 打印方法一 -->结束

// 打印方法二 -->开始 没有使用
// 在主线程下，通过ipcMain对象监听渲染线程传过来的getPrinterList事件
ipcMain.on('getPrinterList', (event) => {
  // 主线程获取打印机列表
  const list = mainWindow.webContents.getPrinters()
  // 通过webContents发送事件到渲染线程，同时将打印机列表也传过去
  mainWindow.webContents.send('getPrinterList', list)
})
// 打印方法二 -->结束

/**
 * Auto Updater 自动更新
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
