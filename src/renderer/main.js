import Vue from 'vue'
import axios from 'axios'
import App from './App'
import router from './router'
import store from './store'
import mixins from './mixins'
import API from './api'
import mainBar from '@/components/main-bar'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.prototype.api = API
Vue.config.productionTip = false

// 全局注册UI
Vue.use(ElementUI)
// 全局注册混合
Vue.mixin(mixins)
// 全局注册组件
Vue.component('main-bar', mainBar)
/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
