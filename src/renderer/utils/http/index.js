
import xhr from './xhr'

const http = {
  async post (url, params) {
    try {
      // console.log('请求地址:', url)
      // console.log('请求参数:', params)
      // 响应信息
      params = JSON.stringify(params)
      const res = await xhr.post(url, params)
      const data = res
      const trancode = data.status
      const tranmsg = data.Text
      const repbody = data.data
      if (trancode === 200) {
        // console.info('交易成功:', repbody)
        return {
          code: '1',
          result: repbody
        }
      } else if (trancode === 500) {
        // console.error('交易异常:', tranmsg)
        return {
          code: '0',
          result: tranmsg
        }
      } else {
        // console.error('交易失败')
        return {
          code: '0',
          result: tranmsg
        }
      }
    } catch (error) {
      // console.error('请求异常:', error)
      return {
        code: '0'
      }
    }
  }
}

http.interceptors = xhr.interceptors

export default http
