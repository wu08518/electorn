import axios from 'axios'

// 项目环境
let host = ''
let port = ''
// api服务器地址:118.24.146.123:8080
// 管理后台地址:118.24.72.96:8080
console.log('NODE_ENV:', process.env.NODE_ENV)
if (process.env.NODE_ENV === 'development') {
  host = '118.24.146.123'
  port = '8080'
} else {
  // 生产环境
  host = '118.24.146.123'
  port = '8080'
}

// 服务器地址
const server = `http://${host}:${port}`
// const server = url
console.log('server address:', server)

const xhr = axios.create({
  baseURL: server,
  transformRequest: [
    data => {
      return data
    }
  ],
  headers: {
    'Content-Type': 'application/json'
    // 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
    // 'Content-Type': 'application/json;charset=utf-8'
  },
  timeout: 30000
})

export default xhr
