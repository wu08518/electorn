import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const index = (resolve) => require(['@/views'], resolve)
const hardware = (resolve) => require(['@/views/hardware'], resolve)
const print = (resolve) => require(['@/views/print'], resolve)
const downfile = (resolve) => require(['@/views/downfile'], resolve)
const readholder = (resolve) => require(['@/views/readholder'], resolve)
const printhander = (resolve) => require(['@/views/printhander'], resolve)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/views/hardware',
      name: 'hardware',
      component: hardware,
      meta: {
        level: 2
      }
    },
    {
      path: '/views/print',
      name: 'print',
      component: print,
      meta: {
        level: 2
      }
    },
    {
      path: '/views/downfile',
      name: 'downfile',
      component: downfile,
      meta: {
        level: 2
      }
    },
    {
      path: '/views/readholder',
      name: 'readholder',
      component: readholder,
      meta: {
        level: 2
      }
    },
    {
      path: '/views/printhander',
      name: 'printhander',
      component: printhander,
      meta: {
        level: 1
      }
    },
    {
      path: '*',
      redirect: '@/views'
    }
  ]
})
